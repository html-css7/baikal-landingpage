// menu
let burger = document.getElementById('burger')
let footerBurger = document.getElementById('footer-burger')
let mobailMenu = document.getElementById('mobail-menu')
let menuClose = document.getElementById('menu-close')
let body = document.querySelector('body')
let feedbackBtn = document.getElementById('feedback-btn')

if (burger && burger !== undefined) {
    burger.addEventListener('click', () => {
        mobailMenu.classList.add('active')
        body.style.overflowY = 'hidden'
    })
}
if (footerBurger && footerBurger !== undefined) {
    footerBurger.addEventListener('click', () => {
        mobailMenu.classList.add('active')
        body.style.overflowY = 'hidden'
    })
}
if (menuClose && menuClose !== undefined) {
    menuClose.addEventListener('click', () => {
        mobailMenu.classList.remove('active')
        body.style.overflowY = 'scroll'
    })
}

if (feedbackBtn && feedbackBtn !== undefined) {
    feedbackBtn.addEventListener('click', () => {
        mobailMenu.classList.remove('active')
        body.style.overflowY = 'scroll'
    })
}

// modal
let feedbackModalBtn = document.getElementById('feedback-modal-btn')
let modalFeedback = document.getElementById('modal-feedback')
let modalFeedbackClose = document.getElementById('modal-feedback-close')

if (feedbackModalBtn && feedbackModalBtn !== undefined) {
    feedbackModalBtn.addEventListener('click', () => {
        modalFeedback.classList.add('active')
        body.style.overflowY = 'hidden'
    })
}
if (modalFeedbackClose && modalFeedbackClose !== undefined) {
    modalFeedbackClose.addEventListener('click', () => {
        modalFeedback.classList.remove('active')
        body.style.overflowY = 'scroll'
    })    
}
