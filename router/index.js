var express = require('express')
var router = express.Router()

router.get('/', function (req, res) {
    res.render('index')
})
router.get('/campsites', (req, res) => {
    res.render('campsites')
})
router.get('/campsite_1', (req, res) => {
    res.render('campsite_1')
})
router.get('/campsite_2', (req, res) => {
    res.render('campsite_2')
})
router.get('/excursions', (req, res) => {
    res.render('excursions_list')
}) 
router.get('/example', (req, res) => {
    res.render('typical_with_long_cards')
})
router.get('/cruises', (req, res) => {
    res.render('cruises')
})
router.get('/map', (req, res) => {
    res.render('map')   
})    
router.get('/gallery', (req, res) => {
    res.render('gallery')   
})
router.get('/contacts', (req, res) => {
    res.render('contacts')   
})

module.exports = router