const express = require('express')
const bodyParser = require('body-parser')
const http = require('http')
const cors = require('cors')
const router = require('./router')

const app = express()
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs')
app.use(express.static('public'))

app.use('/', router)


const PORT = 3000
http.createServer({}, app).listen(PORT)
console.log(`Server running at ${PORT}`)